<?php

namespace Agion\Conneqt\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Sales\Setup\SalesSetupFactory
     */
    private $salesSetupFactory;

    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory,
        \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $externalIdEntityTypes = [
            \Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            \Magento\Catalog\Api\Data\ProductAttributeInterface::ENTITY_TYPE_CODE
        ];

        foreach ($externalIdEntityTypes as $entityType) {
            $eavSetup->addAttribute(
                $entityType,
                'external_id',
                [
                    'type' => 'varchar',
                    'label' => 'External ID',
                    'input' => 'text',
                    'required' => false,
                    'visible' => true,
                    'user_defined' => false,
                    'position' => 1000,
                    'system' => false
                ]
            );
        }

        $eavSetup->addAttribute(
            \Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'parent_external_id',
            [
                'type' => 'varchar',
                'label' => 'Parent External ID',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => false,
                'position' => 1000,
                'system' => false
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            'message_id',
            [
                'type' => 'varchar',
                'label' => 'Message ID',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => false,
                'position' => 1000,
                'system' => false
            ]
        );

        $customerAttributeSetId = $eavSetup->getDefaultAttributeSetId(\Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER);
        $eavSetup->addAttributeToSet(
            \Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            $customerAttributeSetId,
            null,
            'external_id'
        );

        $eavSetup->addAttributeToSet(
            \Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            $customerAttributeSetId,
            null,
            'parent_external_id'
        );

        $eavSetup->addAttributeToSet(
            \Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER,
            $customerAttributeSetId,
            null,
            'message_id'
        );

        $salesSetup = $this->salesSetupFactory->create([
            'resourceName' => 'sales_setup',
            'setup' => $setup
        ]);

        $salesSetup->addAttribute(
            \Magento\Sales\Model\Order::ENTITY,
            'external_id',
            [
                'type' => 'varchar',
                'label' => 'External ID',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => false,
                'position' => 1000,
                'system' => false
            ]
        );

        $salesSetup->addAttribute(
            \Magento\Sales\Model\Order::ENTITY,
            'message_id',
            [
                'type' => 'varchar',
                'label' => 'Message ID',
                'input' => 'text',
                'required' => false,
                'visible' => true,
                'user_defined' => false,
                'position' => 1000,
                'system' => false
            ]
        );

        $setup->endSetup();
    }
}
