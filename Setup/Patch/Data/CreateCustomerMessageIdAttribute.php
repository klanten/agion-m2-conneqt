<?php

namespace Agion\Conneqt\Setup\Patch\Data;

class CreateCustomerMessageIdAttribute implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var \Magento\Eav\Model\Config
     */
    private $eavConfig;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Attribute
     */
    private $attributeResourceModel;
    /**
     * @var \Magento\Customer\Setup\CustomerSetupFactory
     */
    private $customerSetupFactory;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Eav\Model\Config$eavConfig,
        \Magento\Customer\Model\ResourceModel\Attribute $attributeResourceModel
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->eavConfig = $eavConfig;
        $this->attributeResourceModel = $attributeResourceModel;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $eavSetup = $this->customerSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $externalIdEntityTypes = [
            \Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER
        ];

        foreach ($externalIdEntityTypes as $entityType) {
            $eavSetup->addAttribute(
                $entityType,
                'message_id',
                [
                    'type' => 'varchar',
                    'label' => 'Message ID',
                    'input' => 'text',
                    'required' => false,
                    'visible' => true,
                    'user_defined' => false,
                    'position' => 1000,
                    'system' => false
                ]
            );

            $attributeSetId = $eavSetup->getDefaultAttributeSetId(\Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER);
            $attributeGroupId = $eavSetup->getDefaultAttributeGroupId(\Magento\Customer\Api\CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER);

            /** @var \Magento\Eav\Model\Attribute $attribute */
            $attribute = $this->eavConfig->getAttribute($entityType, 'message_id');

            $attribute->setData('attribute_set_id', $attributeSetId);
            $attribute->setData('attribute_group_id', $attributeGroupId);

            $attribute->addData([
                'used_in_forms' => [
                    'adminhtml_customer'
                ]
            ]);

            $this->attributeResourceModel->save($attribute);
        }
    }
}
