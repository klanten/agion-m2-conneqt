<?php

namespace Agion\Conneqt\Plugin;

class SynchronizeAddressesWithParentsAndChilds
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $customerCollectionFactory;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Address\CollectionFactory
     */
    private $customerAddressCollectionFactory;
    /**
     * @var \Magento\Customer\Model\ResourceModel\AddressFactory
     */
    private $customerAddressResourceFactory;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magento\Customer\Model\ResourceModel\Address\CollectionFactory $customerAddressCollectionFactory,
        \Magento\Customer\Model\ResourceModel\AddressFactory $customerAddressResourceFactory
    ) {
        $this->request = $request;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->customerAddressCollectionFactory = $customerAddressCollectionFactory;
        $this->customerAddressResourceFactory = $customerAddressResourceFactory;
    }

    /**
     * @param \Magento\Customer\Api\AddressRepositoryInterface $subject
     * @param  \Magento\Customer\Api\Data\AddressInterface     $result
     * @param \Magento\Customer\Api\Data\AddressInterface      $address
     *
     * @return \Magento\Customer\Api\Data\AddressInterface
     */
    public function afterSave(\Magento\Customer\Api\AddressRepositoryInterface $subject, $result, \Magento\Customer\Api\Data\AddressInterface $address)
    {
        if (strpos($this->request->getPathInfo(), '/rest/') !== false) {
            return $result;
        }

        /** @var \Magento\Customer\Model\Address $savedAddressDbModel */
        $savedAddressDbModel = $this->customerAddressCollectionFactory->create()
            ->addAttributeToSelect(['external_id'])
            ->addFieldToFilter('entity_id', ['eq' => $address->getId()])->getFirstItem();

        /** @var \Magento\Customer\Model\Customer $customer */
        foreach ($this->getCustomersToCheck($savedAddressDbModel) as $customer) {
            $customerAddressResource = $this->customerAddressResourceFactory->create();

            /** @var \Magento\Customer\Model\Address $address */
            foreach ($customer->getAddressesCollection() as $address) {
                if ($this->getMatchableId($savedAddressDbModel->getData('external_id')) === $this->getMatchableId($address->getData('external_id')) && $this->getMatchableId($savedAddressDbModel->getData('external_id')) !== "") {
                    $address->addData([
                        'prefix' => $savedAddressDbModel->getPrefix(),
                        'suffix' => $savedAddressDbModel->getSuffix(),
                        'firstname' => $savedAddressDbModel->getFirstname(),
                        'lastname' => $savedAddressDbModel->getLastname(),
                        'middlename' => $savedAddressDbModel->getMiddlename(),
                        'region_id' => $savedAddressDbModel->getRegionId(),
                        'street' => $savedAddressDbModel->getStreet(),
                        'city' => $savedAddressDbModel->getCity(),
                        'country_id' => $savedAddressDbModel->getCountryId(),
                        'postcode' => $savedAddressDbModel->getPostcode(),
                        'telephone' => $savedAddressDbModel->getTelephone(),
                        'vat_id' => $savedAddressDbModel->getData('vat_id')
                    ]);

                    $customerAddressResource->save($address);
                }
            }
        }

        return $result;
    }

    public function beforeDeleteById(\Magento\Customer\Api\AddressRepositoryInterface $subject, $addressId)
    {
        /** @var \Magento\Customer\Model\Address $savedAddressDbModel */
        $savedAddressDbModel = $this->customerAddressCollectionFactory->create()
            ->addAttributeToSelect(['external_id'])
            ->addFieldToFilter('entity_id', ['eq' => $addressId])->getFirstItem();

        if (strpos($this->request->getPathInfo(), '/rest/') !== false) {
            return $addressId;
        }

        /** @var \Magento\Customer\Model\Customer $customer */
        foreach ($this->getCustomersToCheck($savedAddressDbModel) as $customer) {
            $customerAddressResource = $this->customerAddressResourceFactory->create();

            /** @var \Magento\Customer\Model\Address $address */
            foreach ($customer->getAddressesCollection() as $address) {
                if ($this->getMatchableId($savedAddressDbModel->getData('external_id')) === $this->getMatchableId($address->getData('external_id')) && $this->getMatchableId($savedAddressDbModel->getData('external_id')) !== "") {
                    $customerAddressResource->delete($address);
                }
            }
        }

        return $addressId;
    }

    /**
     * @param $externalId
     *
     * @return string
     */
    private function getMatchableId($externalId)
    {
        $parts = explode("_", $externalId);
        array_shift($parts);
        array_shift($parts);

        return implode("_", $parts);
    }

    private function getCustomersToCheck($savedAddressDbModel)
    {
        $customersToCheck = [];

        $customerDbModel = $this->customerCollectionFactory->create()
            ->addAttributeToSelect(['parent_external_id', 'external_id'])
            ->addFieldToFilter('entity_id', ['eq' => $savedAddressDbModel->getCustomerId()])->getFirstItem();

        if ($customerDbModel->getData('parent_external_id') != null) {
            $matchingCustomerSiblings = $this->customerCollectionFactory->create()
                ->addAttributeToFilter('parent_external_id', ['eq' => $customerDbModel->getData('parent_external_id')])
                ->addFieldToFilter('entity_id', ['neq' => $customerDbModel->getId()])
                ->walk(function ($customer) use (&$customersToCheck) {
                    array_push($customersToCheck, $customer);
                });

            $parentCustomer = $this->customerCollectionFactory->create()
                ->addAttributeToFilter('external_id', ['eq' => $customerDbModel->getData('parent_external_id')])
                ->addFieldToFilter('entity_id', ['neq' => $customerDbModel->getId()])
                ->walk(function ($customer) use (&$customersToCheck) {
                    array_push($customersToCheck, $customer);
                });
        }

        if ($customerDbModel->getData('external_id') != null) {
            $matchingCustomerChilds = $this->customerCollectionFactory->create()
                ->addAttributeToFilter('parent_external_id', ['eq' => $customerDbModel->getData('external_id')])
                ->addFieldToFilter('entity_id', ['neq' => $customerDbModel->getId()])
                ->walk(function ($customer) use (&$customersToCheck) {
                    array_push($customersToCheck, $customer);
                });
        }

        return $customersToCheck;
    }
}
