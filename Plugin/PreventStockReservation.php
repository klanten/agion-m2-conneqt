<?php

namespace Agion\Conneqt\Plugin;

class PreventStockReservation
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    public function __construct(\Magento\Framework\App\RequestInterface $request)
    {
        $this->request = $request;
    }

    public function aroundExecute(
        \Magento\InventorySalesApi\Api\PlaceReservationsForSalesEventInterface $subject,
        callable $proceed,
        array $items,
        \Magento\InventorySalesApi\Api\Data\SalesChannelInterface $salesChannel,
        \Magento\InventorySalesApi\Api\Data\SalesEventInterface $salesEvent
    ) {
        if (strpos($this->request->getPathInfo(), 'conneqt_orders') !== false) {
            return;
        }

        return $proceed($items, $salesChannel, $salesEvent);
    }
}