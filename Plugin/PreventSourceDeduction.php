<?php

namespace Agion\Conneqt\Plugin;

class PreventSourceDeduction
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    public function __construct(\Magento\Framework\App\RequestInterface $request)
    {
        $this->request = $request;
    }

    public function aroundExecute(\Magento\InventorySourceDeductionApi\Model\SourceDeductionServiceInterface $subject, callable $proceed, \Magento\InventorySourceDeductionApi\Model\SourceDeductionRequestInterface $sourceDeductionRequest)
    {
        if (strpos($this->request->getPathInfo(), 'conneqt_orders') !== false) {
            return;
        }

        return $proceed($sourceDeductionRequest);
    }
}