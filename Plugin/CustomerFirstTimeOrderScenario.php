<?php

namespace Agion\Conneqt\Plugin;

class CustomerFirstTimeOrderScenario
{
    /**
     * @var \Magento\Customer\Api\AddressRepositoryInterface
     */
    private $addressRepository;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory
     */
    private $customerCollectionFactory;
    /**
     * @var \Magento\Customer\Model\ResourceModel\Address\CollectionFactory
     */
    private $customerAddressCollectionFactory;
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Customer\Api\AddressRepositoryInterface $addressRepository,
        \Magento\Customer\Model\ResourceModel\Customer\CollectionFactory $customerCollectionFactory,
        \Magento\Customer\Model\ResourceModel\Address\CollectionFactory $customerAddressCollectionFactory
    ) {
        $this->addressRepository = $addressRepository;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->customerAddressCollectionFactory = $customerAddressCollectionFactory;
        $this->request = $request;
    }

    /**
     * @param \Magento\Customer\Api\AddressRepositoryInterface $subject
     * @param                                                  $addressId
     * @param callable                                         $proceed
     *
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundDeleteById(\Magento\Customer\Api\AddressRepositoryInterface $subject, callable $proceed, $addressId)
    {
        if (strpos($this->request->getPathInfo(), '/rest/') === false) {
            return $proceed($addressId);
        }

        $dbAddress = $this->customerAddressCollectionFactory->create()
            ->addAttributeToSelect('external_id')
            ->addAttributeToFilter('entity_id', ['eq' => $addressId])
            ->getFirstItem();
        $dbCustomer = $this->customerCollectionFactory->create()
            ->addFieldToFilter('entity_id', ['eq' => $dbAddress->getCustomerId()])
            ->getFirstItem();

        /** @var \Magento\Customer\Model\Customer $dbCustomer */
        $addressIdentifiers = $this->getAddressIdentifiers($dbCustomer->getAddressesCollection());
        $identifierCounts = [];

        foreach ($addressIdentifiers as $addressIdentifier) {
            if (!array_key_exists($addressIdentifier, $identifierCounts)) {
                $identifierCounts[$addressIdentifier] = 1;
            } else {
                $identifierCounts[$addressIdentifier] += 1;
            }
        }

        if ($identifierCounts[$this->getAddressIdentifier($dbAddress)] > 1 || $dbAddress->getData('external_id')) {
            $proceed($addressId);
        }
    }

    private function getAddressIdentifier($address)
    {
        /**
         * @var \Magento\Customer\Api\Data\AddressInterface $address
         */
        return implode("_", [$address->getPostcode(), $address->getCity()]);
    }

    private function getAddressIdentifiers($addresses)
    {
        $identifiers = [];

        /**
         * @var \Magento\Customer\Api\Data\AddressInterface $address
         */

        foreach ($addresses as $address) {
            array_push($identifiers, implode("_", [$address->getPostcode(), $address->getCity()]));
        }

        return $identifiers;
    }
}
