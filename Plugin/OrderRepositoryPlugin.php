<?php

namespace Agion\Conneqt\Plugin;

class OrderRepositoryPlugin
{
    /**
     * @var \Magento\Sales\Api\Data\OrderExtensionFactory
     */
    private $orderExtensionFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    private $orderCollectionFactory;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order
     */
    private $orderResource;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    private $orderFactory;

    public function __construct(
        \Magento\Sales\Api\Data\OrderExtensionFactory $orderExtensionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order $orderResource,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
        $this->orderExtensionFactory = $orderExtensionFactory;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderResource = $orderResource;
        $this->orderFactory = $orderFactory;
    }

    /**
     * Retrieves order model
     *
     * @param $entityId
     * @return \Magento\Framework\DataObject
     */
    protected function getOrderModel($entityId)
    {
        $order = $this->orderCollectionFactory->create()
            ->addFieldToFilter('entity_id', ['eq' => $entityId])
            ->getFirstItem();

        return $order;
    }

    /**
     * Adds the extension attributes to the order
     *
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function addExtensionAttributesToOrder(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $externalId = $order->getData('external_id') ?? '';
        $messageId = $order->getData('message_id') ?? '';

        $extensionAttributes = $order->getExtensionAttributes() ?? $this->orderExtensionFactory->create();
        $extensionAttributes->setExternalId($externalId);
        $extensionAttributes->setMessageId($messageId);
        $order->setExtensionAttributes($extensionAttributes);

        return $order;
    }

    /**
     * Adds the extension attributes to the order
     *
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $result
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function afterGet(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $result
    ) {
        return $this->addExtensionAttributesToOrder($result);
    }

    /**
     * Adds extension attributes to the order search results
     *
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderSearchResultInterface $result
     * @return \Magento\Sales\Api\Data\OrderSearchResultInterface
     */
    public function afterGetList(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderSearchResultInterface $result
    ) {
        $orders = $result->getItems();

        foreach ($orders as &$order) {
            $this->addExtensionAttributesToOrder($order);
        }

        return $result;
    }

    /**
     * Saves extension attributes to database
     *
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $result
     * @throws \Exception
     */
    public function afterSave(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $result
    ) {
        $order = $this->getOrderModel($result->getEntityId());
        $orderModel = $this->orderFactory->create(['data' => $order->getData()]);

        $extensionAttributes = $result->getExtensionAttributes();

        if ($extensionAttributes && $extensionAttributes->getExternalId()) {
            $orderModel->setData('external_id', $extensionAttributes->getExternalId());
        }

        if ($extensionAttributes && $extensionAttributes->getMessageId()) {
            $orderModel->setData('message_id', $extensionAttributes->getMessageId());
        }

        $this->orderResource->save($orderModel);

        return $result;
    }
}